package com.ob.music.trimmer;

import android.Manifest;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.ob.music.ui.adapter.Pager;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MusicMainActivity extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks {
    private static final String TAG = "MusicMainActivity";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ConnectivityManager connectivityManager;
    private static final int REQUEST_CODE_PERMISSION = 123;
    private String tabTitles[] = new String[] {"Sever", "Device"};

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.Viewpager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);

        Pager pagerAdapter = new Pager(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        methodRequirePermission();
        connectionCheck();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void methodRequirePermission() {
        String[] str = {Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, str)) {
            Log.i(TAG, "PERMISSION_GRANTED");

        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "Music App Would Require Permission",
                    REQUEST_CODE_PERMISSION,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    private void connectionCheck() {
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean iswifi = networkInfo.isConnected();

        NetworkInfo networkInfo1 =
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean ismobile = networkInfo1.isConnected();

        Log.i(TAG, "Wifi--" + iswifi);
        Log.i(TAG, "Mobile--" + ismobile);
        if (iswifi) {
            Toast.makeText(this, "Connection Successfully With Wifi !!  ", Toast.LENGTH_LONG)
                    .show();
        } else {
            if (ismobile) {
                Toast.makeText(
                                this,
                                "Connection Successfully With Mobile Data!!  ",
                                Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(this, "Connection Failed  ", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Some permissions have been granted
        Log.i(TAG, "PERMISSION_GRANTED");
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        Log.i(TAG, "PERMISSION_DENIED");
    }
}
