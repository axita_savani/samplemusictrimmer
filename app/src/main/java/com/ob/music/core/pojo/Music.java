package com.ob.music.core.pojo;

/**
 * Created by Razil on 28/07/2018.
 */
public class Music {
    public int counter;
    public long track_Id;
    public String track_Title;
    public String track_data;
    public String track_displayName;
    public long track_duration;

    public Music setCounter(int counter) {
        this.counter = counter;
        return this;
    }

    public int getCounter() {
        return counter;
    }

    public Music setTrack_Id(long track_Id) {
        this.track_Id = track_Id;
        return this;
    }

    public long getTrack_Id() {
        return track_Id;
    }

    public Music setTrack_data(String track_data) {
        this.track_data = track_data;
        return this;
    }

    public String getTrack_data() {
        return track_data;
    }

    public Music setTrack_Title(String track_Title) {
        this.track_Title = track_Title;
        return this;
    }

    public String getTrack_Title() {
        return track_Title;
    }

    public Music setTrack_displayName(String track_displayName) {
        this.track_displayName = track_displayName;
        return this;
    }

    public String getTrack_displayName() {
        return track_displayName;
    }

    public Music setTrack_duration(long track_duration) {
        this.track_duration = track_duration;
        return this;
    }

    public long getTrack_duration() {
        return track_duration;
    }

}
