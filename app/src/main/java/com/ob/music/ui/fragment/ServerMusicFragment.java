package com.ob.music.ui.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jean.jcplayer.general.JcStatus;
import com.example.jean.jcplayer.general.errors.OnInvalidPathListener;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.service.JcPlayerManagerListener;
import com.example.jean.jcplayer.view.JcPlayerView;
import com.ob.music.trimmer.R;
import com.ob.music.ui.adapter.AudioAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;

/** Created by Razil on 28/07/2018. */
public class ServerMusicFragment extends Fragment
        implements OnInvalidPathListener, JcPlayerManagerListener {
    private static final String TAG = "ServerMusicFragment";
    private JcPlayerView jcPlayer;
    private RecyclerView recyclerServermusic;
    Context context;
    private AudioAdapter audioAdapterserver;

    private ArrayList<String> title = new ArrayList<>();
    private ArrayList<JcAudio> arrayMusic;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayMusic = new ArrayList<>();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_server_music, container, false);
        this.jcPlayer = Objects.requireNonNull(getActivity()).findViewById(R.id.player);
        this.recyclerServermusic = view.findViewById(R.id.RecycleJsonMusic);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /////////////////////////// Get music list/////////////////////
        GetAllMediaMp3Files();
    }

    // json catched from file
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String loadfromasset() {
        String json;
        try {
            InputStream is = Objects.requireNonNull(getActivity()).getAssets().open("music.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer);
            //  Log.i(TAG, "loadfromasset()--STRING JSON-->" + json + "--------");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        jcPlayer.kill();
    }

    @Override
    public void onPathError(JcAudio jcAudio) {}

    @Override
    public void onPreparedAudio(JcStatus jcStatus) {

        Log.i(TAG, "onPreparedAudio()" + arrayMusic.size());
        Log.i(TAG, "onPreparedAudio() SERVER" + arrayMusic);
    }

    @Override
    public void onCompletedAudio() {}

    @Override
    public void onPaused(JcStatus jcStatus) {}

    @Override
    public void onContinueAudio(JcStatus jcStatus) {}

    @Override
    public void onPlaying(JcStatus jcStatus) {

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onTimeChanged(final JcStatus jcStatus) {
        //   updateProgress(jcStatus);
    }

    /* @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        private void updateProgress(final JcStatus jcStatus) {
            Log.d(
                    TAG,
                    "Song id = "
                            + jcStatus.getJcAudio().getId()
                            + ", song duration = "
                            + jcStatus.getDuration()
                            + "\n song position = "
                            + jcStatus.getCurrentPosition());

            Objects.requireNonNull(getActivity())
                    .runOnUiThread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    // calculate progress
                                    float progress =
                                            (float)
                                                            (jcStatus.getDuration()
                                                                    - jcStatus.getCurrentPosition())
                                                    / (float) jcStatus.getDuration();
                                    progress = 1.0f - progress;

                                    //   audioAdapterserver.updateProgress(jcStatus.getJcAudio(),
                                    // progress);

                                }
                            });
        }
    */
    @Override
    public void onJcpError(Throwable throwable) {}

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void GetAllMediaMp3Files() {

        try {
            JSONObject obj = new JSONObject(loadfromasset());

            JSONArray musicarray = obj.getJSONArray("Music");

            for (int i = 0; i < musicarray.length(); i++) {
                JSONObject music = musicarray.getJSONObject(i);
                title.add(music.getString("title"));

                String title = music.getString("title");
                String url = music.getString("url");

                arrayMusic.add(JcAudio.createFromURL(title, url));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        jcPlayer.initPlaylist(arrayMusic, this);
        Log.i(TAG,"serveradapter"+arrayMusic);
        serveradapter();
    }

    protected void serveradapter() {

        audioAdapterserver = new AudioAdapter(arrayMusic);
        Log.i(TAG, "TAG-->" + arrayMusic.size());
        audioAdapterserver.setOnItemClickListener(
                new AudioAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {

                        jcPlayer.playAudio(arrayMusic.get(position));

                    }
                });
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerServermusic.setLayoutManager(layoutManager);
        recyclerServermusic.setAdapter(audioAdapterserver);
        ((SimpleItemAnimator) recyclerServermusic.getItemAnimator())
                .setSupportsChangeAnimations(false);
    }
}
