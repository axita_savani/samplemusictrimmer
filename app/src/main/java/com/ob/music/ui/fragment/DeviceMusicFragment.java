package com.ob.music.ui.fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jean.jcplayer.general.JcStatus;
import com.example.jean.jcplayer.general.errors.OnInvalidPathListener;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.service.JcPlayerManagerListener;
import com.example.jean.jcplayer.view.JcPlayerView;
import com.ob.music.trimmer.R;
import com.ob.music.ui.adapter.AudioAdapter;

import java.util.ArrayList;
import java.util.Objects;

/** Created by Razil on 28/07/2018. */
public class DeviceMusicFragment extends Fragment
        implements OnInvalidPathListener ,JcPlayerManagerListener{
    private static final String TAG = "DeviceMusicFragment";

    private JcPlayerView jcPlayer;
    private RecyclerView recyclerViewmusic;
    private AudioAdapter audioAdapter;
    ContentResolver contentResolver;
    ArrayList<JcAudio> musiclist;
    Context context;
    Cursor cursor;
    Uri uri;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        musiclist = new ArrayList<>();


    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_device_music, container, false);

        this.jcPlayer = Objects.requireNonNull(getActivity()).findViewById(R.id.player);
        this.recyclerViewmusic = view.findViewById(R.id.recyclerViewMusic);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /////////////////////////// Get music list/////////////////////
        GetAllMediaMp3Files();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        jcPlayer.kill();
    }



    @Override
    public void onPathError(JcAudio jcAudio) {}



    /* @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateProgress(final JcStatus jcStatus) {
        Log.d(
                TAG,
                "Song id = "
                        + jcStatus.getJcAudio().getId()
                        + ", song duration = "
                        + jcStatus.getDuration()
                        + "\n song position = "
                        + jcStatus.getCurrentPosition());

        Objects.requireNonNull(getActivity())
                .runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                // calculate progress
                                float progress =
                                        (float)
                                                        (jcStatus.getDuration()
                                                                - jcStatus.getCurrentPosition())
                                                / (float) jcStatus.getDuration();
                                progress = 1.0f - progress;
                                // audioAdapter.updateProgress(jcStatus.getJcAudio(), progress);

                            }
                        });
    }*/


    public void GetAllMediaMp3Files() {
        //  ArrayList<JcAudio> jcAudios = new ArrayList<>();

        contentResolver = context.getContentResolver();
        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        cursor =
                contentResolver.query(
                        uri, // Uri
                        null, null, null, null);
        if (cursor == null) {
            Log.i(TAG, "Cursor null");
        } else if (!cursor.moveToFirst()) {
            Log.i(TAG, "SD CArd not avalible");
        } else {
            int Title = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int i = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            do {
                String SongTitle = cursor.getString(Title);
                String data = cursor.getString(i);

                musiclist.add(JcAudio.createFromFilePath(SongTitle, data));

            } while (cursor.moveToNext());

            jcPlayer.initPlaylist(musiclist,this);
            Log.i(TAG,"GetAllMediaMp3Files()"+musiclist.size());
            adapterSetup();
        }
    }

    protected void adapterSetup() {

        audioAdapter = new AudioAdapter(musiclist);
        //  Log.i(TAG, " PLAYER LIST" + jcPlayer.getMyPlaylist());
        audioAdapter.setOnItemClickListener(
                new AudioAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        //  Log.i(TAG,"MUSIC DEVICE LIST-->"+musiclist);
                        jcPlayer.playAudio(musiclist.get(position));

                    }
                });
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewmusic.setLayoutManager(layoutManager);
        recyclerViewmusic.setAdapter(audioAdapter);
        ((SimpleItemAnimator) recyclerViewmusic.getItemAnimator())
                .setSupportsChangeAnimations(false);
    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void onPreparedAudio(JcStatus jcStatus) {
        Log.i(TAG,"onPreparedAudio()"+musiclist.size());
        Log.i(TAG,"onPreparedAudio()"+musiclist);
    }

    @Override
    public void onPaused(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudio(JcStatus jcStatus) {

    }

    @Override
    public void onPlaying(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChanged(JcStatus jcStatus) {

    }

    @Override
    public void onJcpError(Throwable throwable) {

    }
}
