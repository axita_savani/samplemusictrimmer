package com.ob.music.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ob.music.ui.fragment.DeviceMusicFragment;
import com.ob.music.ui.fragment.ServerMusicFragment;

/** Created by Razil on 28/07/2018. */
public class Pager extends FragmentStatePagerAdapter {

    // integer to count number of tabs

    // Constructor to the class
    public Pager(FragmentManager fm) {
        super(fm);
    }

    // Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        // Returning the current tabs
        switch (position) {
            case 0:
                return new ServerMusicFragment();
            case 1:
                return new DeviceMusicFragment();

            default:
                return null;
        }
    }

    // Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Server";
        } else if (position == 1) {
            title = "Device";
        }
        return title;
    }
}
