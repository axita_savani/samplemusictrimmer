package com.ob.music.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jean.jcplayer.JcPlayerManager;
import com.example.jean.jcplayer.model.JcAudio;
import com.ob.music.trimmer.R;

import java.util.ArrayList;
import java.util.List;

/** Created by Razil on 30/07/2018. */
public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioAdapterViewHolder> {
    public static final String TAG = "AudioAdapter";
    private OnItemClickListener mListener;
    private List<JcAudio> jcAudioList;
    private SparseArray<Float> progressMap = new SparseArray<>();
    private Context context;
    private JcPlayerManager jcPlayerManager;

    public AudioAdapter(List<JcAudio> jcAudioList) {
   //     this.jcAudioList=new ArrayList<>();
        this.jcAudioList=jcAudioList;
        setHasStableIds(true);
    }

    // Define the method that allows the parent activity or fragment to define the
    // jcPlayerManagerListener
    public void setOnItemClickListener(OnItemClickListener listener) {

      this.mListener = listener;
        Log.i(TAG, "LISTENER" + mListener);
    }

    @Override
    public long getItemId(int position) {
        //  Log.i(TAG,"getItemId()"+jcAudioList.get(position).getId());
        return jcAudioList.get(position).hashCode();
    }

    @Override
    public AudioAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_list_music, parent, false);
        return new AudioAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AudioAdapterViewHolder holder, int position) {
        String title = jcAudioList.get(position).getTitle();
        //  String title = jcAudioList.get(position);
        holder.audioTitle.setText(title);
      //  holder.itemView.setTag(jcAudioList.get(position));


        holder.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Triggers click upwards to the adapter on click
                        if (mListener != null) {
                            mListener.onItemClick(holder.getAdapterPosition());

                            Log.i(TAG, "getAdapterPosition()" + holder.getAdapterPosition());
                        }
                    }
                });
        applyProgressPercentage(holder, progressMap.get(position, 0.0f));
    }
    /**
     * Applying percentage to progress.
     *
     * @param holder ViewHolder
     * @param percentage in float value. where 1 is equals as 100%
     */
    private void applyProgressPercentage(AudioAdapterViewHolder holder, float percentage) {
        Log.d(TAG, "applyProgressPercentage() with percentage = " + percentage);
        LinearLayout.LayoutParams progress =
                (LinearLayout.LayoutParams) holder.viewProgress.getLayoutParams();
        LinearLayout.LayoutParams antiProgress =
                (LinearLayout.LayoutParams) holder.viewAntiProgress.getLayoutParams();

        progress.weight = percentage;
        holder.viewProgress.setLayoutParams(progress);

        antiProgress.weight = 1.0f - percentage;
        holder.viewAntiProgress.setLayoutParams(antiProgress);
    }

    @Override
    public int getItemCount() {
        return jcAudioList == null ? 0 : jcAudioList.size();
    }

    // Define the mListener interface
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    static class AudioAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView audioTitle;
        private View viewProgress;
        private View viewAntiProgress;

        public AudioAdapterViewHolder(View view) {
            super(view);
            this.audioTitle = view.findViewById(R.id.txtMusicTitle);
            viewProgress = view.findViewById(R.id.song_progress_view);
            viewAntiProgress = view.findViewById(R.id.song_anti_progress_view);
        }
    }

    public void updateProgress(JcAudio jcAudio, float progress) {
        int position = jcAudioList.indexOf(jcAudio);
        Log.d(TAG, "Progress = " + progress);

        progressMap.put(position, progress);
        if (progressMap.size() > 1) {
            for (int i = 0; i < progressMap.size(); i++) {
                if (progressMap.keyAt(i) != position) {
                    Log.d(TAG, "KeyAt(" + i + ") = " + progressMap.keyAt(i));
                    notifyItemChanged(progressMap.keyAt(i));
                    progressMap.delete(progressMap.keyAt(i));
                }
            }
        }
       // Log.i(TAG,"ADAPTER"+jcAudioList);
        notifyItemChanged(position);
    }
}
